var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var validateEmail = function(email) {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};

var userSchema = new Schema({

  first_name: { type: String,  required: [true, 'First name must be provided'] },

    last_name: { type: String,  required: [true, 'last name must be provided'] },

  email:    { 
    
    type: String,     

    Required:  'Email address cannot be left blank.',
    validate: [validateEmail, 'Please fill a valid email address'],
         match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address'],
    index: {unique: true, dropDups: true}
    },

  phone: { type: String , required: [true,  'Phone cannot be left blank']},

  

  country: { type: String , required: [true, 'Country cannot be left blank.']},

  state: { type: String,  required: [true, 'state must be provided'] },
});

module.exports = mongoose.model('Users', userSchema);
