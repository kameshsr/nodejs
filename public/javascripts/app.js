
$(function(){

    $("#register").on('click', function(event){
        event.preventDefault();
        var firstname   = $("#firstname").val();
        var lastname   = $("#lastname").val();
        var email      = $("#email").val();
        var phone   = $("#phone").val();
        var country    = $("#country").val();
        var state   = $("#state").val();
        var terms      = $('input[name="terms"]:checked').val();

        if(!firstname || !lastname || !email || !phone || !country || !state){ 
            $("#msgDiv").show().html("All fields are required.");
        }  else if (!terms){
            $("#msgDiv").show().html("Please agree to terms and conditions.");
        }
        else{ 
            $.ajax({
                url: "/register",
                method: "POST",
                data: { first_name: firstname, last_name: lastname, email: email, phone: phone, country: country, state: state, terms: terms }
            }).done(function( data ) {

                if ( data ) {
                    if(data.status == 'error'){

                        var errors = '<ul>';
                        $.each( data.message, function( key, value ) {
                            errors = errors +'<li>'+value.msg+'</li>';
                        });

                        errors = errors+ '</ul>';
                        $("#msgDiv").html(errors).show();
                    }else{
                        $("#msgDiv").removeClass('alert-danger').addClass('alert-success').html(data.message).show(); 
                    }
                }
            });
        }
    });
});
