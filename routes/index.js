var express   = require('express');
var router    = express.Router();
var mongoose  = require('mongoose');
var User      = mongoose.model('Users');
var crypto    = require('crypto'), hmac, signature;
const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize }   = require('express-validator/filter');

   /* GET home page. */
  router.get('/', function(req, res, next) {
      res.render('index', { title: 'Nodejs user registration'});
   })
   router.get('/view', function(req, res, next) {
    res.render('view')
})
  /* POST user registration page. */
  router.post('/register',[ 
   
    check('first_name','First Name cannot be left blank')
    .isLength({ min: 1 }),
    check('last_name','Last Name cannot be left blank')
    .isLength({ min: 1 }),
   
    check('email')
    .isEmail().withMessage('Please enter a valid email address')
    .trim()
    .normalizeEmail()
    .custom(value => {
        return findUserByEmail(value).then(User => {
          //if user email already exists throw an error
      })
    }),

    check('phone')
    .isLength({ min: 10 }).withMessage('Phone must be at least 10 chars long')
    .matches(/\d/).withMessage('Phone must contain  number'),

  
    check('country','Country cannot be left blank')
    .isLength({ min: 1 }),
    check('state','state cannot be left blank')
    .isLength({ min: 1 }),
    
    check('terms','Please accept our terms and conditions').equals('yes'),

   ], function(req, res, next) {

      const errors = validationResult(req);

    if (!errors.isEmpty()) {     
        
       res.json({status : "error", message : errors.array()});

    } else {

        hmac = crypto.createHmac("sha1", 'auth secret');
        

        
        var document = {
            first_name:  req.body.first_name,
            last_name:   req.body.last_name, 
            email:       req.body.email, 
            phone:       req.body.phone, 
            country:     req.body.country, 
            state:      req.body.state, 
            
          };
        
        var user = new User(document); 
        user.save(function(error){
          console.log(user);
          if(error){ 
            throw error;
          }
             res.redirect('/view')
          
       });    
    }
});

function findUserByEmail(email){

  if(email){
      return new Promise((resolve, reject) => {
        User.findOne({ email: email })
          .exec((err, doc) => {
            if (err) return reject(err)
            if (doc) return reject(new Error('This email already exists. Please enter another email.'))
            else return resolve(email)
          })
      })
    }
 }


module.exports = router;
